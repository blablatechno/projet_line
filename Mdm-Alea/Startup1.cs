﻿using System;
using System.Threading.Tasks;
using Microsoft.Owin;
using Microsoft.Owin.Cors;
using Owin;

[assembly: OwinStartup(typeof(Mdm_Alea.Startup1))]

namespace Mdm_Alea
{
    public class Startup1
    {
        public void Configuration(IAppBuilder app)
        {
            // Changer le nom du chat, lancer le signalR et permettre à tout le monde d'y accéder
            app.Map("/petitchat", map =>
            {
                map.UseCors(CorsOptions.AllowAll);
                map.RunSignalR();
            });
        }
    }
}
