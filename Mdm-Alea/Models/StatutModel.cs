﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Mdm_Alea.Models
{
    public class StatutModel
    {
        public int Id { get; set; }
        public string Nom { get; set; }
        public int Education { get; set; }
        public int Revenu { get; set; }
        public int Profession { get; set; }
    }
}