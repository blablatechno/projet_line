﻿using AleaDAL.Entities;
using AleaDAL.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Mdm_Alea.Models
{
    public class SearchFormModel
    {
        public string NomPerso { get; set; }
        public List<int> PickedStatut { get; set; }

        private IEnumerable<Statut> _AllStatuts;
        public IEnumerable<Statut> AllStatuts
        {
            get
            {
                if(_AllStatuts == null)
                {
                    StatutService service = new StatutService();
                    _AllStatuts = service.GetAll();
                }
                return _AllStatuts;
            }
        }
    }
}