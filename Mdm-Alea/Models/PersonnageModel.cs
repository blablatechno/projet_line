﻿using AleaDAL.Entities;
using AleaDAL.Repository;
using Mdm_Alea.Mapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Mdm_Alea.Models
{
    public class PersonnageModel
    {
        public int Id { get; set; }
        public string Nom { get; set; }
        public string Description { get; set; }
        public int StatutId { get; set; }
        public int? Sante
        {
            get
            {

                PersonnageService service = new PersonnageService();
                return service.GetSante(this.Id);
            }
            set{ }

        }
        //private string _Statut;
        //public string Statut
        //{
        //    get
        //    {
        //        StatutService service = new StatutService();
        //        return _Statut = service.GetStatutByPerso(StatutId).Nom;
        //    }
        //}

        private IEnumerable<Ressources> _Ressources;
        public IEnumerable<Ressources> Ressources
        {
            get
            {
                RessourcesService service = new RessourcesService();
                _Ressources = service.GetRessourcesByPerso(this.Id);              
                return _Ressources;
            }
        }

        private IEnumerable<Objectifs> _Objectifs;
        public IEnumerable<Objectifs> Objectifs
        {
            get

            {
                ObjectifsService service = new ObjectifsService();
                _Objectifs = service.GetObjectifsByPerso(this.Id);
                return _Objectifs;
            }
        }




        private IEnumerable<Besoins> _Besoins;
        public IEnumerable<Besoins> Besoins
        {
            get
            {
                BesoinsService service = new BesoinsService();
                _Besoins = service.GetBesoinsByPerso(this.Id);
                return _Besoins;
            }
        }
    }
}