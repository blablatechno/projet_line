﻿using AleaDAL.Entities;
using AleaDAL.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Mdm_Alea.Models
{
    public class EvenementModel
    {
        public int Id { get; set; }
        public string Nom { get; set; }
        public string Description { get; set; }
        public int CategorieId { get; set; }
        private IEnumerable<Categorie> _Categorie;
        public IEnumerable<Categorie> Categorie
        {
            get
            {
                CategorieService categorieService = new CategorieService();
                _Categorie = categorieService.GetAll();
                return _Categorie;
            }
        }
        private IEnumerable<Ressources> _Ressources;
        public IEnumerable<Ressources> Ressources
        {
            get
            {
                if (_Ressources == null)
                {
                    RessourcesService ressource = new RessourcesService();
                    _Ressources = ressource.GetAll();
                }
                return _Ressources.ToList();
            }
            set
            {
                _Ressources = value;
            }
        }
        public IEnumerable<int> Quantite { get; set; }
        public bool General { get; set; }
    }
}