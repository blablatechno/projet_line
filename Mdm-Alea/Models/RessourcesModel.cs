﻿using AleaDAL.Entities;
using AleaDAL.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Mdm_Alea.Models
{
    public class RessourcesModel
    {
        public IEnumerable<Ressources> Ressources { get; set; }
        public int Id { get; set; }
        public string Nom { get; set; }
        public int AutreId { get; set; }
        public int Quantite { get; set; }
    }
}