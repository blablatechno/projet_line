﻿using AleaDAL.Entities;
using AleaDAL.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Mdm_Alea.Models
{
    public class InsertPersoModel
    {
        public int Id { get; set; }
        public string Nom { get; set; }
        public string Description { get; set; }
        public int Sante { get; }

        public List<int> PickedObjectifs { get; set; }
        public List<int> PickedBesoins { get; set; }

        private List<Ressources> _Ressources;
        public List<Ressources> Ressources
        {
            get
            {
                // Lazy loading
                if (_Ressources == null) {

                    RessourcesService service = new RessourcesService();
                    _Ressources = service.GetAll().ToList();
                }


                return _Ressources;
            }
        }

        private IEnumerable<Objectifs> _Objectifs;
        public IEnumerable<Objectifs> Objectifs
        {
            get

            {
                ObjectifsService service = new ObjectifsService();
                _Objectifs = service.GetAll();
                return _Objectifs.ToList();
            }
        }

        private IEnumerable<Besoins> _Besoins { get; set; }
        public IEnumerable<Besoins> Besoins
        {
            get
            {
                BesoinsService service = new BesoinsService();
                _Besoins = service.GetAll();
                return _Besoins;
            }
        }

        private IEnumerable<Statut> _Statut { get; set; }
        public IEnumerable<Statut> Statut
        {
            get
            {
                StatutService service = new StatutService();
                _Statut = service.GetAll();
                return _Statut;
            }
        }
        public int StatutId { get; set; }


    }
}