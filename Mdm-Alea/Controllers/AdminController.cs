﻿using AleaDAL;
using AleaDAL.Entities;
using AleaDAL.Repository;
using Mdm_Alea.Mapper;
using Mdm_Alea.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;


namespace Mdm_Alea.Controllers
{
    public class AdminController : Controller
    {

        [HttpPost]
        public ActionResult InsertPerso(InsertPersoModel model)
        {
            PersonnageService service = new PersonnageService();
            int modelid = service.Insert(model.ToPersoInsert());

            BesoinsService besoinsService = new BesoinsService();
            foreach (int besoinsId in model.PickedBesoins)
            {
                besoinsService.InitBesoinbyPerso(modelid, besoinsId);
            }

            RessourcesService ressourcesService = new RessourcesService();
            foreach (Ressources item in model.Ressources)
            {
                service.InitQuantiteRessource(item.Quantite, modelid, item.Id);
            }

            ObjectifsService objectifsService = new ObjectifsService();
            IEnumerable<Objectifs> allObjectifs = objectifsService.GetAll();

            foreach (int objectifId in model.PickedObjectifs)
            {
                objectifsService.InitObjectifbyPerso(modelid, objectifId);
            }

            service.Sante(model.StatutId, modelid);
            TempData["SuccessMessage"] = "Personnage bien inséré.";
            return View(new InsertPersoModel());
        }

        [HttpGet]
        public ActionResult InsertPerso()
        {
            return View(new InsertPersoModel());
        }

        [HttpGet]
        public ActionResult InsertObjectif()
        {
            return View(new ObjectifModel());
        }

        [HttpPost]
        public ActionResult InsertObjectif(ObjectifModel model)
        {
            ObjectifsService objectifsServices = new ObjectifsService();
            int modelid = objectifsServices.Insert(model.Map<Objectifs>());
            foreach (Ressources item in model.Ressources)
            {
                objectifsServices.QuantiteRessource(item.Quantite, modelid, item.Id);
            }
            TempData["SuccessMessage"] = "Objectif bien inséré.";

            return View(new ObjectifModel());
        }

        [HttpGet]
        public ActionResult InsertBesoin()
        {
            return View(new BesoinModel());
        }

        [HttpPost]
        public ActionResult InsertBesoin(BesoinModel model)
        {
            BesoinsService services = new BesoinsService();
            int modelid = services.Insert(model.Map<Besoins>());
            foreach (Ressources item in model.Ressources)
            {
                services.QuantiteRessource(item.Quantite, modelid, item.Id);
            }
            TempData["SuccessMessage"] = "Besoin bien inséré.";

            return View(new BesoinModel());
        }


        [HttpGet]
        public ActionResult InsertEvenement()
        {
            return View(new EvenementModel());
        }

        [HttpPost]
        public ActionResult InsertEvenement(EvenementModel model)
        {
            EvenementService service = new EvenementService();
            int modelid = service.Insert(model.Map<Evenement>());
            foreach (Ressources item in model.Ressources)
            {
                service.InitConsequences(item.Quantite, modelid, item.Id);
            }
            return View(new EvenementModel());
        }

        [HttpGet]
        public ActionResult InsertRessource()
        {
            return View(new RessourcesModel());
        }

        [HttpPost]
        public ActionResult InsertRessource(RessourcesModel model)
        {
            RessourcesService service = new RessourcesService();
            service.Insert(model.Map<Ressources>());
            TempData["SuccessMessage"] = "Ressource bien insérée.";

            return View(new RessourcesModel());
        }

        [HttpGet]
        public ActionResult InsertCategorie()
        {
            return View(new CategorieModel());
        }

        [HttpPost]
        public ActionResult InsertCategorie(CategorieModel model)
        {
            CategorieService categorieService = new CategorieService();
            categorieService.Insert(model.Map<Categorie>());
            TempData["SuccessMessage"] = "Catégorie bien inséré.";

            return View(new CategorieModel());
        }

        [HttpGet]
        public ActionResult InsertStatut()
        {
            return View(new StatutModel());
        }

        [HttpPost]
        public ActionResult InsertStatut(StatutModel model)
        {
            StatutService statutService = new StatutService();
            statutService.Insert(model.Map<Statut>());
            TempData["SuccessMessage"] = "Statut bien inséré.";
            return View(new StatutModel());
        }

        public ActionResult SearchPerso()
        {
            return View(new SearchFormModel());
        }

        [HttpPost]
        public ActionResult AjaxSearch(SearchFormModel model)
        {
            PersonnageService service = new PersonnageService();
            IEnumerable<Personnage> allPersonnages = service.GetAll();

            IEnumerable<PersonnageModel> result = allPersonnages.Where(
                p => p.Nom.ToLower().Contains(model.NomPerso?.ToLower() ?? string.Empty)
                ).Select(p => p.Map<PersonnageModel>());
            if (model.PickedStatut != null)
            {
                result = result.Where(r => model.PickedStatut.Contains(r.StatutId));
            }
            return PartialView(result);
        }

    }
}