﻿using AleaDAL;
using AleaDAL.Repository;
using Mdm_Alea.Mapper;
using Mdm_Alea.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Mdm_Alea.Controllers
{
    public class PersoController : Controller
    {
        // GET: Perso

        public ActionResult ListePerso()
        {
            PersonnageService service = new PersonnageService();
            IEnumerable<Personnage> perso = service.GetAll();
            List<PersonnageModel> model = new List<PersonnageModel>();
            foreach (Personnage p in perso)
            {
                model.Add(p.ToPersoModel());
            }
            return View(model);
        }

        public ActionResult Perso(int id)
        {
            PersonnageService personnageservice = new PersonnageService();
            PersonnageModel model = personnageservice.Get(id).ToPersoModel();
            return View(model);
        }
    }
}