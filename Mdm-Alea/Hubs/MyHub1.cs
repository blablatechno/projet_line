﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using Microsoft.AspNet.SignalR;

namespace Mdm_Alea
{
    public class MyHub1 : Hub
    {
        public static List<string> Connection = new List<string>();

        public override Task OnConnected()
        {
            if (!Connection.Contains(Context.ConnectionId))
            {
                Connection.Add(Context.ConnectionId);
            }
            return base.OnConnected();
        }

        public override Task OnDisconnected(bool stopCalled)
        {

            Connection.Remove(Context.ConnectionId);

            return base.OnDisconnected(stopCalled);
        }

        public void MessageATous(string message)
        {
            Clients.All.sendMessage(message);
        }

        public void ListConnection()
        {
            Clients.Caller.showConnections(Connection);
        }

        public void MessagePrive(string message, string connectionid)
        {
            Clients.Client(connectionid).messagePrive(message);
        }


    }
}