﻿CREATE PROCEDURE [dbo].[SP_Register]
	@email NVARCHAR(255), 
	@MotDePasse NVARCHAR(255), 
	@Prenom NVARCHAR(50), 
	@Nom NVARCHAR(50), 
	@DateNaissance DATETIME2

AS
	INSERT INTO Utilisateur(Email, MotDePasse, Prenom, Nom, DateNaissance)
	OUTPUT inserted.ID
	VALUES (@email, dbo.UDF_Hash_Password(@MotDePasse, @email), @Nom, @Prenom, @DateNaissance);

RETURN 0
