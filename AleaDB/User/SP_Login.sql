﻿CREATE PROCEDURE [dbo].[SP_Login]
	@email NVARCHAR(255), 
	@MotDePasse NVARCHAR(255)
AS
	SELECT *
	FROM [Utilisateur]
	WHERE Email = @email AND [MotDePasse] = dbo.UDF_Hash_Password(@MotDePasse, @email);

RETURN 0
