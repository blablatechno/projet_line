﻿CREATE TABLE [dbo].[Utilisateur]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY,
    [Nom] NVARCHAR(50) NOT NULL, 
    [Prenom] NVARCHAR(50) NOT NULL, 
    [DateNaissance] DATETIME2 NULL, 
    [Email] NVARCHAR(50) NOT NULL, 
    [MotDePasse] VARBINARY(50) NOT NULL, 
    [AccesId] INT NOT NULL REFERENCES Acces
)
