﻿CREATE TABLE [dbo].[Besoins_Ressou]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY, 
	[BesoinsId] INT NOT NULL REFERENCES Besoins,
	[RessouId] INT NOT NULL REFERENCES Ressources, 
	[Quantite] INT NOT NULL
)
