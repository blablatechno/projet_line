﻿CREATE TABLE [dbo].[Perso_Ressou]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY,
    [Quantite] INT NOT NULL DEFAULT 1, 
    [PersoId] INT NULL REFERENCES Personnage,
    [RessouId] INT NULL REFERENCES Ressources
)
