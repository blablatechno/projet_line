﻿CREATE TABLE [dbo].[Perso_Statut]
(
	[Id] INT NOT NULL PRIMARY KEY, 
    [PersoId] INT NULL REFERENCES Personnage, 
    [StatutId] INT NULL REFERENCES Statut
)
