﻿CREATE PROCEDURE [dbo].[SP_Valider_objectifs]
@objectifid int,
@quantite int, 
@ressourcesid int, 
@persoid int
AS
	BEGIN 
	UPDATE Objectifs SET Statut = 1 WHERE Id = @objectifid
	END
	BEGIN
	UPDATE Perso_Ressou SET Quantite = @quantite WHERE (@quantite = (SELECT Quantite FROM Objectif_Ressou 
																		JOIN Objectifs ON Objectif_Ressou.ObjectifsId = Objectifs.Id 
																		JOIN Ressources ON Objectif_Ressou.RessourcesId = Ressources.Id)) 
															AND (Perso_Ressou.PersoId = @persoid) 
															AND (Perso_Ressou.RessouId = @ressourcesid)
	END

