﻿CREATE TABLE [dbo].[Perso_Event]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY,
    [PersoId] INT NULL REFERENCES Personnage, 
    [EventId] INT NULL REFERENCES Evenement
)
