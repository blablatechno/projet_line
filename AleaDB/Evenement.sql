﻿CREATE TABLE [dbo].[Evenement]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY,
    [Nom] NVARCHAR(50) NOT NULL, 
    [Description] NVARCHAR(MAX) NOT NULL,
    [CategorieId] INT NOT NULL REFERENCES Categorie, 
    [General] BIT NOT NULL DEFAULT 0 
	
)
