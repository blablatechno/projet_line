﻿CREATE TABLE [dbo].[Besoins]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY,
    [Nom] NVARCHAR(50) NOT NULL, 
    [Description] NVARCHAR(MAX) NULL, 
    [Statut] BIT NOT NULL DEFAULT 0, 
    [Temps] INT NULL, 
    [CategorieId] INT NOT NULL REFERENCES Categorie
	)
