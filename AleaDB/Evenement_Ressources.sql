﻿CREATE TABLE [dbo].[Evenement_Ressources]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY, 
    [EvenementId] INT NOT NULL REFERENCES Evenement, 
    [RessourcesId] INT NOT NULL REFERENCES Ressources, 
	[Quantite] INT NOT NULL
)
