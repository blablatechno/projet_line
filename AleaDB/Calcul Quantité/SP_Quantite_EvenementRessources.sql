﻿CREATE PROCEDURE [dbo].[SP_Quantite_EvenementRessources]
	@quantite int, 
	@ressourceId int, 
	@evenementId int
AS
	INSERT INTO Evenement_Ressources (Quantite, RessourcesId, EvenementId) VALUES (@quantite, @ressourceId, @evenementId)
RETURN 0
