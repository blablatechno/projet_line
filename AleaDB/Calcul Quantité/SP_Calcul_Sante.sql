﻿CREATE PROCEDURE [dbo].[SP_Calcul_Sante]
@statutid int, 
@personnageid int
AS
  UPDATE Personnage SET Sante = (SELECT Revenu + Education + Profession FROM Statut WHERE Statut.Id = @statutid) WHERE Personnage.Id = @personnageid
  
  RETURN 0
