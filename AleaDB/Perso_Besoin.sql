﻿CREATE TABLE [dbo].[Perso_Besoin]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY,
    [PersoId] INT NULL REFERENCES Personnage,
    [BesoinId] INT NULL REFERENCES Besoins
)
