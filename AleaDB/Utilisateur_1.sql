﻿CREATE TABLE [dbo].[Utilisateur]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY,
    [Nom] NVARCHAR(50) NULL, 
    [Prenom] NVARCHAR(50) NULL, 
    [DateNaissance] DATETIME2 NULL, 
    [Email] NVARCHAR(50) NULL, 
    [MotDePasse] VARBINARY(50) NULL, 
    [AccesId] INT NULL REFERENCES Acces(AccesId)
)
