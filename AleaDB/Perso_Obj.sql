﻿CREATE TABLE [dbo].[Perso_Obj]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY,
    [PersoId] INT NULL REFERENCES Personnage, 
    [ObjId] INT NULL REFERENCES Objectifs
)
