﻿CREATE TABLE [dbo].[Objectif_Ressou]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY, 
	[ObjectifsId] INT NOT NULL REFERENCES Objectifs,
	[RessourcesId] INT NOT NULL REFERENCES Ressources, 
	[Quantite] INT NOT NULL
)
