﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AleaDAL.Entities
{
    public class Ressources
    {
        public int Id { get; set; }
        public string Nom { get; set; }
        public int Quantite { get; set; }
    }
}
