﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AleaDAL.Entities
{
    public class Besoins_Ressou
    {
        public int Id { get; set; }
        public int BesoinId { get; set; }
        public int RessouId { get; set; }
        public int Quantite { get; set; }
    }
}
