﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AleaDAL.Entities
{
    public class Acces
    {
        public int Id { get; set; }
        public int AccesId { get; set; }
        public string Nom { get; set; }
    }
}
