﻿using AleaDAL.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AleaDAL
{
    public class Personnage
    {
        public int Id { get; set; }
        public string Nom { get; set; }
        public string Description { get; set; }
        public int? Sante { get; set; }
        public int? StatutId { get; set; }

    }
}


    