﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AleaDAL.Entities
{
    public class Perso_obj
    {
        public int Id { get; set; }
        public int PersoId { get; set; }
        public int ObjId { get; set; }
    }
}
