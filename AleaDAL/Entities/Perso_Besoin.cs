﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AleaDAL.Entities
{
    public class Perso_Besoin
    {
        public int Id { get; set; }
        public int PersoId { get; set; }
        public int BesoinId { get; set; }
    }
}
