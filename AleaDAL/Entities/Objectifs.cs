﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AleaDAL.Entities
{
    public class Objectifs
    {
        public int Id { get; set; }
        public string Nom { get; set; }
        public string Description { get; set; }
        public bool Statut { get; set; }
        public int CategorieId { get; set; }
    }
}
