﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AleaDAL.Entities
{
    public class Statut
    {
        public int Id { get; set; }
        public string Nom { get; set; }
        public int Education { get; set; }
        public int Revenu { get; set; }
        public int Profession { get; set; }
        public int Sante
        {
            get
            {
                return Sante = Education + Revenu + Profession;
            }
            set
            { }
        }
    }
}
