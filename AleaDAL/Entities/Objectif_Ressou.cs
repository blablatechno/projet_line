﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AleaDAL.Entities
{
    public class Objectif_Ressou
    {
        public int Id { get; set; }
        public int ObjectifsId { get; set; }
        public int RessourcesId { get; set; }
        public int Quantite { get; set; }
    }
}
