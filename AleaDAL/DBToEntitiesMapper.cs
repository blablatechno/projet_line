﻿using AleaDAL.Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AleaDAL
{
    class DBToEntitiesMapper
    {
     public static Acces ToAcces (IDataReader reader)
        {
            return new Acces
            {
                Id = (int)reader["Id"],
                Nom = (string)reader["Nom"], 
                AccesId = (int)reader["AccesId"]
            };
        }


        public static Besoins ToBesoins (IDataReader reader)
        {
            return new Besoins
            {
                Id = (int)reader["Id"],
                Nom = reader["Nom"].ToString(),
                Description = reader["Description"].ToString(),
                Statut = (bool)reader["Statut"], 
                Temps = reader["Temps"] as int?, 
                CategorieId = (int)reader["CategorieId"]
            };
        }

        public static Categorie ToCategorie (IDataReader reader)
        {
            return new Categorie
            {
                Id = (int)reader["Id"],
                Nom = reader["Nom"].ToString()
            };
        }


        public static Evenement ToEvenement (IDataReader reader)
        {
            return new Evenement
            {
                Id = (int)reader["Id"],
                Nom = reader["Nom"].ToString(),
                Description = reader["Description"].ToString(),
                CategorieId = (int)reader["CategorieId"], 
                General = (bool)reader["General"]
            };
        }

        public static Objectifs ToObjectifs (IDataReader reader)
        {
            return new Objectifs
            {
                Id = (int)reader["Id"],
                Nom = (string)reader["Nom"],
                Description = reader["Description"].ToString(),
                Statut = (bool)reader["Statut"],
                CategorieId = (int)reader["CategorieId"]
            };
        }

        public static Personnage ToPersonnage (IDataReader reader)
        {
            return new Personnage
            {
                Id = (int)reader["Id"],
                Nom = reader["Nom"].ToString(),
                Description = reader["Description"].ToString(),
                Sante = reader["Sante"] as int ?,
                StatutId = reader["StatutId"] as int?
            };
        }

        public static Ressources ToRessources (IDataReader reader)
        {
            return new Ressources
            {
                Id = (int)reader["Id"], 
                Nom = reader["Nom"].ToString()
            };
        }

        public static Statut ToStatut (IDataReader reader)
        {
            return new Statut
            {
                Id = (int)reader["Id"],
                Nom = reader["Nom"].ToString(),
                Education = (int)reader["Education"], 
                Revenu = (int)reader["Revenu"], 
                Profession = (int)reader["Profession"]
            };
        }


        public static Utilisateur ToUtilisateur (IDataReader reader)
        {
            return new Utilisateur
            {
                Id = (int)reader["Id"], 
                Nom = reader["Nom"].ToString(),
                Prenom = reader["Prenom"].ToString(),
                DateNaissance = (DateTime)reader["DateNaissance"], 
                Email = reader["Email"].ToString(),
                AccesId = (int)reader["AccesId"]
            };
        }
    }
}
