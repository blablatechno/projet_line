﻿using AleaDAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToolBox;

namespace AleaDAL.Repository
{
    public class CategorieService : BaseService<Categorie>
    {
        public override bool Delete(int id)
        {
            string query = "DELETE FROM Categorie WHERE id = @id";
            Command cmd = new Command(query);
            cmd.Parameters.Add("@id", id);
            return _Connection.ExecuteNonQuery(cmd) == 1;
        }

        public override Categorie Get(int id)
        {
            string query = "SELECT * FROM Categorie WHERE Id = @id";
            Command cmd = new Command(query);
            cmd.Parameters.Add("@id", id);
            return _Connection.ExecuteReader(cmd, DBToEntitiesMapper.ToCategorie).FirstOrDefault();
        }

        public override IEnumerable<Categorie> GetAll()
        {
            Command cmd = new Command("SELECT * FROM Categorie");
            return _Connection.ExecuteReader(cmd, DBToEntitiesMapper.ToCategorie);
        }

        public override int Insert(Categorie item)
        {
            string query = "INSERT INTO Categorie (Nom) OUTPUT inserted.Id  VALUES (@Nom) ";
            Command cmd = new Command(query);
            cmd.Parameters.Add("@Nom", item.Nom);
            return (int)_Connection.ExecuteScalar(cmd);
        }

        public override bool Update(Categorie item)
        {
            string query = "UPDATE Categorie SET Nom = @Nom WHERE Id=@id";
            Command cmd = new Command(query);
            cmd.Parameters.Add("@Nom", item.Nom);
            cmd.Parameters.Add("@Id", item.Id);
            return (int)_Connection.ExecuteNonQuery(cmd) == 1;
        }
    }
}
