﻿using AleaDAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToolBox;

namespace AleaDAL.Repository
{
    public class AccesService : BaseService<Acces>
    {
        public override bool Delete(int id)
        {
            string query = "DELETE FROM Acces WHERE id = @id";
            Command cmd = new Command(query);
            cmd.Parameters.Add("@id", id);
            return _Connection.ExecuteNonQuery(cmd) == 1;
        }

        public override Acces Get(int id)
        {
            string query = "SELECT * FROM Acces WHERE Id = @id";
            Command cmd = new Command(query);
            cmd.Parameters.Add("@id", id);
            return _Connection.ExecuteReader(cmd, DBToEntitiesMapper.ToAcces).FirstOrDefault();
        }

        public override IEnumerable<Acces> GetAll()
        {
            Command cmd = new Command("SELECT * FROM Acces");
            return _Connection.ExecuteReader(cmd, DBToEntitiesMapper.ToAcces);
        }

        public override int Insert(Acces item)
        {
            string query = "INSERT INTO Acces (AccesId, Nom)  OUTPUT inserted.Id  VALUES (@AccesId, @Nom ) ";
            Command cmd = new Command(query);
            cmd.Parameters.Add("@AccesId", item.AccesId);
            cmd.Parameters.Add("@Nom", item.Nom);
            return (int)_Connection.ExecuteScalar(cmd);
        }

        public override bool Update(Acces item)
        {
            string query = "UPDATE Acces SET AccesId = @AccesId, Nom = @Nom  WHERE Id=@id";
            Command cmd = new Command(query);
            cmd.Parameters.Add("@FirstName", item.AccesId);
            cmd.Parameters.Add("@LastName", item.Nom);
            cmd.Parameters.Add("Id", item.Id);
            return (int)_Connection.ExecuteNonQuery(cmd) == 1;
        }
    }
}
