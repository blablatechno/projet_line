﻿using AleaDAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToolBox;

namespace AleaDAL.Repository
{
    public class BesoinsService : BaseService<Besoins>
    {
        public override bool Delete(int id)
        {
            string query = "DELETE FROM Besoins WHERE id = @id";
            Command cmd = new Command(query);
            cmd.Parameters.Add("@id", id);
            return _Connection.ExecuteNonQuery(cmd) == 1;
        }

        public override Besoins Get(int id)
        {
            string query = "SELECT * FROM Besoins WHERE Id = @id";
            Command cmd = new Command(query);
            cmd.Parameters.Add("@id", id);
            return _Connection.ExecuteReader(cmd, DBToEntitiesMapper.ToBesoins).FirstOrDefault();
        }

        public override IEnumerable<Besoins> GetAll()
        {
            Command cmd = new Command("SELECT * FROM Besoins");
            return _Connection.ExecuteReader(cmd, DBToEntitiesMapper.ToBesoins);
        }

        public override int Insert(Besoins item)
        {
            string query = "INSERT INTO Besoins (Nom, Description, Temps, CategorieId) OUTPUT inserted.Id VALUES (@Nom, @Description, @Temps, @CategorieId ) ";
            Command cmd = new Command(query);
            cmd.Parameters.Add("@Nom", item.Nom);
            cmd.Parameters.Add("@Description", (object)item.Description ?? DBNull.Value );
            cmd.Parameters.Add("@Temps", (object)item.Temps ?? DBNull.Value);
            cmd.Parameters.Add("@CategorieId", item.CategorieId);
            return (int)_Connection.ExecuteScalar(cmd);
        }

        public override bool Update(Besoins item)
        {
            string query = "UPDATE Besoins SET Nom = @Nom, Description = @Description, Temps = @Temps, CategorieId = @CategorieId  WHERE Id=@id";
            Command cmd = new Command(query);
            cmd.Parameters.Add("@Nom", item.Nom);
            cmd.Parameters.Add("@Description", (object)item.Description ?? DBNull.Value);
            cmd.Parameters.Add("@Temps", (object)item.Temps ?? DBNull.Value);
            cmd.Parameters.Add("@CategorieId", item.CategorieId);
            cmd.Parameters.Add("@Id", item.Id);
            return (int)_Connection.ExecuteNonQuery(cmd) == 1;
        }

        public int QuantiteRessource(int quantite, int besoinId, int ressouId)
        {

            Command cmd = new Command("INSERT INTO Besoins_Ressou(Quantite, BesoinsId, RessouId) OUTPUT inserted.Id VALUES(@quantite, @besoinId, @ressouId)");
            cmd.AddParameter("@quantite", quantite);
            cmd.AddParameter("@besoinId", besoinId);
            cmd.AddParameter("@ressouId", ressouId);
            return (int)_Connection.ExecuteScalar(cmd);
        }

        public IEnumerable<Besoins> GetBesoinsByPerso(int id)
        {
            string query = "SELECT Nom, [Description], Statut, Temps, Id, CategorieId FROM Besoins b JOIN Perso_Besoin p ON b.Id = p.BesoinId WHERE p.PersoId = @id";
            Command cmd = new Command(query);
            cmd.Parameters.Add("@id", id);
            return _Connection.ExecuteReader(cmd, DBToEntitiesMapper.ToBesoins);
        }

        public int InitBesoinbyPerso(int idperso, int idbesoin)
        {
            string query = "INSERT INTO Perso_Besoin (PersoId, BesoinId) OUTPUT inserted.id VALUES (@idperso, @idbesoin)";
            Command cmd = new Command(query);
            cmd.AddParameter("@idperso", idperso);
            cmd.AddParameter("@idbesoin", idbesoin);
            return (int)_Connection.ExecuteScalar(cmd);
        }
    }
}
