﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AleaDAL.Entities;
using ToolBox;

namespace AleaDAL.Repository
{
    public class PersonnageService : BaseService<Personnage>
    {
        public override bool Delete(int id)
        {
            string query = "DELETE FROM Personnage WHERE id = @id";
            Command cmd = new Command(query);
            cmd.Parameters.Add("@id", id);
            return _Connection.ExecuteNonQuery(cmd) == 1;
        }

        public override Personnage Get(int id)
        {
            string query = "SELECT * FROM Personnage WHERE Id = @id";
            Command cmd = new Command(query);
            cmd.Parameters.Add("@id", id);
            return _Connection.ExecuteReader(cmd, DBToEntitiesMapper.ToPersonnage).FirstOrDefault();
        }

        public override IEnumerable<Personnage> GetAll()
        {
            Command cmd = new Command("SELECT * FROM Personnage");
            return _Connection.ExecuteReader(cmd, DBToEntitiesMapper.ToPersonnage);
        }

        public override int Insert(Personnage item)
        {
            string query = "INSERT INTO Personnage (Nom, Description)  OUTPUT inserted.Id  VALUES (@Nom, @Description) ";
            Command cmd = new Command(query);
            cmd.Parameters.Add("@Nom", item.Nom);
            cmd.Parameters.Add("@Description", (object)item.Description ?? DBNull.Value);
            return (int)_Connection.ExecuteScalar(cmd);
        }

        public override bool Update(Personnage item)
        {
            string query = "UPDATE Personnage SET Nom = @Nom, Description = @Description, Sante = @Sante WHERE Id = @Id";
            Command cmd = new Command(query);
            cmd.Parameters.Add("@Nom", item.Nom);
            cmd.Parameters.Add("@Description", (object)item.Description ?? DBNull.Value);
            cmd.Parameters.Add("@Sante", item.Sante);
            cmd.Parameters.Add("@Id", item.Id);
            return (int)_Connection.ExecuteNonQuery(cmd) == 1;
        }

        public bool InitQuantiteRessource(int Quantite, int persoId, int ressourceId)
        {
            Command cmd = new Command("INSERT INTO Perso_Ressou (Quantite, PersoId, RessouId) VALUES (@Quantite, @persoId, @ressourceId)");
            cmd.AddParameter("@Quantite", Quantite);
            cmd.AddParameter("@persoId", persoId);
            cmd.AddParameter("@ressourceId", ressourceId);
            return (int)_Connection.ExecuteNonQuery(cmd) == 1;
        }

        public int GetQuantiteRessource (int persoId, int ressId)
        {
            Command cmd = new Command("SELECT COALESCE(SUM(Quantite),0) FROM Perso_Ressou WHERE PersoId = @persoId AND RessouId=@ressId");
            cmd.AddParameter("@PersoId", persoId);
            cmd.AddParameter("@RessId", ressId);
            return (int)_Connection.ExecuteScalar(cmd);
        }
        
        public bool Sante (int statutid, int persoid)
        {
            Command cmd = new Command("UPDATE Personnage SET Sante = (SELECT Revenu + Education + Profession " +
                "FROM Statut WHERE Statut.Id = @statutid) WHERE Personnage.Id = @personnageid");
            cmd.AddParameter("@statutid", statutid);
            cmd.AddParameter("@personnageid", persoid);
            return _Connection.ExecuteNonQuery(cmd) == 1;
        }

        public int GetSante (int Id)
        {
            Command cmd = new Command("SELECT Sante FROM Personnage WHERE Id = @Id");
            cmd.AddParameter("@Id", Id);
            return (int)_Connection.ExecuteScalar(cmd);
        }

        public bool ValiderBesoin (int besoinid, int quantite, int ressourcesid, int persoid)
        {
            Command cmd = new Command("SP_Valider_besoins");
            cmd.AddParameter("@besoinid", besoinid);
            cmd.AddParameter("@quantite", quantite);
            cmd.AddParameter("@ressourcesid", ressourcesid);
            cmd.AddParameter("@persoid", persoid);
            return (int)_Connection.ExecuteNonQuery(cmd) == 1;
        }

        public bool ValiderObjectif (int objectifid, int quantite, int ressourcesid, int persoid)
        {
            Command cmd = new Command("SP_Valider_objectifs");
            cmd.AddParameter("@objectifid", objectifid);
            cmd.AddParameter("@quantite", quantite);
            cmd.AddParameter("@ressourcesid", ressourcesid);
            cmd.AddParameter("@persoid", persoid);
            return (int)_Connection.ExecuteNonQuery(cmd) == 1;
        }
    }
}


