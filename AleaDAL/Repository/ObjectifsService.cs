﻿using AleaDAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToolBox;

namespace AleaDAL.Repository
{
    public class ObjectifsService : BaseService<Objectifs>
    {
        public override bool Delete(int id)
        {
            string query = "DELETE FROM Objectifs WHERE id = @id";
            Command cmd = new Command(query);
            cmd.Parameters.Add("@id", id);
            return _Connection.ExecuteNonQuery(cmd) == 1;
        }

        public override Objectifs Get(int id)
        {
            string query = "SELECT * FROM Objectifs WHERE Id = @id";
            Command cmd = new Command(query);
            cmd.Parameters.Add("@id", id);
            return _Connection.ExecuteReader(cmd, DBToEntitiesMapper.ToObjectifs).FirstOrDefault();
        }

        public override IEnumerable<Objectifs> GetAll()
        {
            Command cmd = new Command("SELECT * FROM Objectifs");
            return _Connection.ExecuteReader(cmd, DBToEntitiesMapper.ToObjectifs);
        }

        public override int Insert(Objectifs item)
        {
            string query = "INSERT INTO Objectifs (Nom, Description, Statut, CategorieId)  OUTPUT inserted.Id  VALUES (@Nom, @Description, @Statut, @CategorieId) ";
            Command cmd = new Command(query);
            cmd.Parameters.Add("@Nom", item.Nom);
            cmd.Parameters.Add("@Description", (object)item.Description ?? DBNull.Value);
            cmd.Parameters.Add("@Statut", item.Statut);
            cmd.Parameters.Add("@CategorieId", (object)item.CategorieId ?? DBNull.Value);
            return (int)_Connection.ExecuteScalar(cmd);
        }

        public override bool Update(Objectifs item)
        {
            string query = "UPDATE Objectifs SET Nom = @Nom, Description = @Description, Statut = @Statut, CategorieId = @CategorieId WHERE Id=@id";
            Command cmd = new Command(query);
            cmd.Parameters.Add("@Nom", item.Nom);
            cmd.Parameters.Add("@Description", (object)item.Description ?? DBNull.Value);
            cmd.Parameters.Add("@Statut", item.Statut);
            cmd.Parameters.Add("@CategorieId", (object)item.CategorieId ?? DBNull.Value);
            cmd.Parameters.Add("@Id", item.Id);
            return (int)_Connection.ExecuteNonQuery(cmd) == 1;
        }

        public bool QuantiteRessource(int Quantite, int ObjectifsId, int RessourcesId)
        {
            Command cmd = new Command("INSERT INTO Objectif_Ressou (Quantite, RessourcesId, ObjectifsId) VALUES (@quantite, @ressourcesId, @objectifsId)");
            cmd.AddParameter("@Quantite", Quantite);
            cmd.AddParameter("@RessourcesId", RessourcesId);
            cmd.AddParameter("@ObjectifsId", ObjectifsId);
            return (int)_Connection.ExecuteNonQuery(cmd) == 1;
        }

        public IEnumerable<Objectifs> GetObjectifsByPerso(int id)
        {
            string query = "SELECT o.* FROM Objectifs o JOIN Perso_Obj p ON p.ObjId = o.Id WHERE PersoId = @id";
            Command cmd = new Command(query);
            cmd.Parameters.Add("@id", id);
            return _Connection.ExecuteReader(cmd, DBToEntitiesMapper.ToObjectifs);
        }

        public int InitObjectifbyPerso(int idperso, int idobjectif)
        {
            string query = "INSERT INTO Perso_Obj (PersoId, ObjId) OUTPUT inserted.id VALUES (@idperso, @idobjectif)";
            Command cmd = new Command(query);
            cmd.AddParameter("@idperso", idperso);
            cmd.AddParameter("@idobjectif", idobjectif);
            return (int)_Connection.ExecuteScalar(cmd);
        }
    }
}