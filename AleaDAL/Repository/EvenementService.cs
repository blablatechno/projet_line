﻿using AleaDAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToolBox;

namespace AleaDAL.Repository
{
    public class EvenementService : BaseService<Evenement>
    {
        public override bool Delete(int id)
        {
            string query = "DELETE FROM Evenement WHERE id = @id";
            Command cmd = new Command(query);
            cmd.Parameters.Add("@id", id);
            return _Connection.ExecuteNonQuery(cmd) == 1;
        }

        public override Evenement Get(int id)
        {
            string query = "SELECT * FROM Evenement WHERE Id = @id";
            Command cmd = new Command(query);
            cmd.Parameters.Add("@id", id);
            return _Connection.ExecuteReader(cmd, DBToEntitiesMapper.ToEvenement).FirstOrDefault();
        }

        public override IEnumerable<Evenement> GetAll()
        {
            Command cmd = new Command("SELECT * FROM Evenement");
            return _Connection.ExecuteReader(cmd, DBToEntitiesMapper.ToEvenement);
        }

        public override int Insert(Evenement item)
        {
            string query = "INSERT INTO Evenement (Nom, Description,  CategorieId, General)  OUTPUT inserted.Id  VALUES (@Nom, @Description, @CategorieId, @General) ";
            Command cmd = new Command(query);
            cmd.Parameters.Add("@Nom", item.Nom);
            cmd.Parameters.Add("@Description", item.Description);
            cmd.Parameters.Add("@CategorieId", item.CategorieId);
            cmd.Parameters.Add("@General", item.General);
            return (int)_Connection.ExecuteScalar(cmd);
        }

        public override bool Update(Evenement item)
        {
            string query = "UPDATE Evenement SET Nom = @Nom, Description = @Description, CategorieId = @CategorieId, General = @General WHERE Id=@id";
            Command cmd = new Command(query);
            cmd.Parameters.Add("@Nom", item.Nom);
            cmd.Parameters.Add("@Description", item.Description);
            cmd.Parameters.Add("@CategorieId", item.CategorieId);
            cmd.Parameters.Add("@General", item.General);
            cmd.Parameters.Add("@Id", item.Id);
            return (int)_Connection.ExecuteNonQuery(cmd) == 1;
        }

        public bool InitConsequences (int Quantite, int EvenementId, int RessourcesId)
        {
            Command cmd = new Command("INSERT INTO Evenement_Ressources (Quantite, RessourcesId, EvenementId) VALUES (@quantite, @ressourceid, @evenementid)");
            cmd.AddParameter("@Quantite", Quantite);
            cmd.AddParameter("@RessourceId", RessourcesId);
            cmd.AddParameter("@EvenementId", EvenementId);
            return (int)_Connection.ExecuteNonQuery(cmd) == 1;
        }

    }
}

