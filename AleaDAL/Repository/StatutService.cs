﻿using AleaDAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToolBox;

namespace AleaDAL.Repository
{
    public class StatutService : BaseService<Statut>
    {
        public override bool Delete(int id)
        {
            string query = "DELETE FROM Statut WHERE id = @id";
            Command cmd = new Command(query);
            cmd.Parameters.Add("@id", id);
            return _Connection.ExecuteNonQuery(cmd) == 1;
        }

        public override Statut Get(int id)
        {
            string query = "SELECT * FROM Statut WHERE Id = @id";
            Command cmd = new Command(query);
            cmd.Parameters.Add("@id", id);
            return _Connection.ExecuteReader(cmd, DBToEntitiesMapper.ToStatut).FirstOrDefault();
        }

        public override IEnumerable<Statut> GetAll()
        {
            Command cmd = new Command("SELECT * FROM Statut");
            return _Connection.ExecuteReader(cmd, DBToEntitiesMapper.ToStatut);
        }

        public override int Insert(Statut item)
        {
            string query = "INSERT INTO Statut (Nom, Education, Revenu, Profession)  OUTPUT inserted.Id  VALUES (@Nom, @Education, @Revenu, @Profession) ";
            Command cmd = new Command(query);
            cmd.Parameters.Add("@Education", item.Education);
            cmd.Parameters.Add("@Revenu", item.Revenu);
            cmd.Parameters.Add("@Profession", item.Profession);
            cmd.Parameters.Add("@Nom", item.Nom);
            return (int)_Connection.ExecuteScalar(cmd);
        }

        public override bool Update(Statut item)
        {
            string query = "UPDATE Statut SET Nom = @Nom, Education = @Education, Revenu = @Revenu, Profession = @Profession WHERE Id = @Id";
            Command cmd = new Command(query);
            cmd.Parameters.Add("@Education", item.Education);
            cmd.Parameters.Add("@Nom", item.Nom);
            cmd.Parameters.Add("@Revenu", item.Revenu);
            cmd.Parameters.Add("@Profession", item.Profession);
            cmd.Parameters.Add("@Id", item.Id);
            return (int)_Connection.ExecuteNonQuery(cmd) == 1;
        }

        //public Statut GetStatutByPerso (int statutid)
        //{
        //    string query = "SELECT * FROM Statut JOIN Personnage ON StatutId = Statut.Id WHERE Statut.Id = @statutid";
        //    Command cmd = new Command(query);
        //    cmd.AddParameter("@Id", statutid);
        //    return _Connection.ExecuteReader(cmd, DBToEntitiesMapper.ToStatut).FirstOrDefault();
        //}
    }
}
