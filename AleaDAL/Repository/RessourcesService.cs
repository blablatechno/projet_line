﻿using AleaDAL.Entities;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToolBox;

namespace AleaDAL.Repository
{
    public class RessourcesService : BaseService<Ressources>
    {
        public override bool Delete(int id)
        {
            string query = "DELETE FROM Ressources WHERE id = @id";
            Command cmd = new Command(query);
            cmd.Parameters.Add("@id", id);
            return _Connection.ExecuteNonQuery(cmd) == 1;
        }

        public override Ressources Get(int id)
        {
            string query = "SELECT * FROM Ressources WHERE Id = @id";
            Command cmd = new Command(query);
            cmd.Parameters.Add("@id", id);
            return _Connection.ExecuteReader(cmd, DBToEntitiesMapper.ToRessources).FirstOrDefault();
        }

        public override IEnumerable<Ressources> GetAll()
        {
            Command cmd = new Command("SELECT * FROM Ressources");
            return _Connection.ExecuteReader(cmd, DBToEntitiesMapper.ToRessources);
        }

        public override int Insert(Ressources item)
        {
            string query = "INSERT INTO Ressources (Nom) OUTPUT inserted.Id VALUES (@Nom) ";
            Command cmd = new Command(query);
            cmd.Parameters.Add("@Nom", item.Nom);
            return (int)_Connection.ExecuteScalar(cmd);
        }

        public override bool Update(Ressources item)
        {
            string query = "UPDATE Ressources SET Nom = @Nom WHERE Id = @Id";
            Command cmd = new Command(query);
            cmd.Parameters.Add("@Nom", item.Nom);
            cmd.Parameters.Add("@Id", item.Id);
            return (int)_Connection.ExecuteNonQuery(cmd) == 1;
        }

        public IEnumerable<Ressources> GetRessourcesByPerso(int id)
        {
            string query = "SELECT COALESCE(SUM(Quantite),0) as Quantite, RessouId as Id, " +
                "Nom as Nom FROM Perso_Ressou JOIN Ressources ON RessouId=Ressources.Id WHERE PersoId = id GROUP BY RessouId, Nom";
            Command cmd = new Command(query);
            cmd.AddParameter("@PersoId", id);
            return _Connection.ExecuteReader(cmd, DBToEntitiesMapper.ToRessources);
        }


    }
}
