﻿using AleaDAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToolBox;

namespace AleaDAL.Repository
{
    public class UtilisateurService : BaseService<Utilisateur>
    {
        public override bool Delete(int id)
        {
            string query = "DELETE FROM Utilisateur WHERE id = @id";
            Command cmd = new Command(query);
            cmd.Parameters.Add("@id", id);
            return _Connection.ExecuteNonQuery(cmd) == 1;
        }

        public override Utilisateur Get(int id)
        {
            string query = "SELECT * FROM Utilisateur WHERE Id = @id";
            Command cmd = new Command(query);
            cmd.Parameters.Add("@id", id);
            return _Connection.ExecuteReader(cmd, DBToEntitiesMapper.ToUtilisateur).FirstOrDefault();
        }

        public override IEnumerable<Utilisateur> GetAll()
        {
            Command cmd = new Command("SELECT * FROM Utilisateur");
            return _Connection.ExecuteReader(cmd, DBToEntitiesMapper.ToUtilisateur);
        }

        public override int Insert(Utilisateur item)
        {
            string query = "INSERT INTO Utilisateur (Nom, Prenom, DateNaissance, Email, MotDePasse, AccesId)  OUTPUT inserted.Id  VALUES (@Nom, @Prenom, @DateNaissance, @Email, @MotDePasse, @AccesId) ";
            Command cmd = new Command(query);
            cmd.Parameters.Add("@Nom", item.Nom);
            cmd.Parameters.Add("@Prenom", item.Prenom);
            cmd.Parameters.Add("@DateNaissance", (object)item.DateNaissance ?? DBNull.Value);
            cmd.Parameters.Add("@Email", item.Email);
            cmd.Parameters.Add("@MotDePasse", item.MotDePasse);
            cmd.Parameters.Add("@accesId",item.AccesId);
            return (int)_Connection.ExecuteScalar(cmd);
        }

        public override bool Update(Utilisateur item)
        {
            string query = "UPDATE Utilisateur SET Nom = @Nom, Prenom=@Prenom, DateNaissance=@DateNaissance, Email=@Email, MotDePasse=@MotDePasse, AccesId = @AccesId";
            Command cmd = new Command(query);
            cmd.Parameters.Add("@Nom", item.Nom);
            cmd.Parameters.Add("@Prenom", item.Prenom);
            cmd.Parameters.Add("@DateNaissance", (object)item.DateNaissance ?? DBNull.Value);
            cmd.Parameters.Add("@Email", item.Email);
            cmd.Parameters.Add("@MotDePasse", item.MotDePasse);
            cmd.Parameters.Add("@accesId", item.AccesId);
            cmd.AddParameter("@Id", item.Id);
            return (int)_Connection.ExecuteNonQuery(cmd) == 1;
        }

        public Utilisateur Login (string email, string password)
        {
            Command cmd = new Command("SP_Login", true);
            cmd.AddParameter("@email", email);
            cmd.AddParameter("@MotDePasse", password);
            return _Connection.ExecuteReader(cmd, DBToEntitiesMapper.ToUtilisateur).FirstOrDefault();
        }
    }
}
